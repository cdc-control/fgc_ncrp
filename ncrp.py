"""The FGC NCRP command is the standard command for communicating with FGC devices. It is a text-based command, with a simple syntax. The command is sent to the device, and the device responds with a response. The response is either a successful response or an error response. 

* It can have a tag, which is a string of up to 31 characters. The tag is optional and may be omitted.
* It must have a verb between s and g, for set and get respectively. The verb is mandatory.
* It can specify a device (if not, it should default to 0),
* It must specify a property. The property must be a valid property. The property can be a single property or a list of properties. Currently, only a single property is supported.
* It can have a transaction ID. The transaction ID can be omitted.
* It can have a user selector. The user selector can be omitted.
* It can have an array specifier.  The array can be a single index, a range, or a range with a step. The array can be omitted, in which case it is assumed to be the entire array.
*  It can have a payload. The payload can be a list of values or options. The payload can be omitted, in which case it is assumed to be an empty list.
"""

from . import errors


class Command:
  """This is the class for defining an FGC NCRP command.
  This class is meant to represent a command, and is not meant to be used for parsing or validating commands. The command is validated in the parser, and the parser returns a command object. The command object is then used to create a response object, which is then sent back to the client.
  The class runs basic checks on the input, but does not check if the command is valid. It is assumed that the command is valid, and that the parser has checked the command. The parser will raise an error if the command is invalid.

  The command object is initialized with the following parameters and checks:
    - tag: should be 0-31 characters, a-z, A-Z, 0-9, _. Optional
    - verb: should be s or g, case insensitive. Mandatory
    - device: can either be a 32 bit unsigned integeror a string of up to 23 characters, starting with A-Z. Optional, if missing 0 is assumed
    - NCRP_property: is not checked here, if it matched it should be a valid property. It may not exist in the device, but that is not an issue on the command level. Though, a property should be specified. Mandatory
    - transaction_id: should be an unsigned integer (no range specified). Optional
    - user_selector: can only be a number between 0 and 31. Optional
    - array: can be a single index, a start index, a range, or a range with a step
    - payload: can be a list of values or options. The payload can be omitted, in which case it is assumed to be an empty list.
  """

  def __init__(self, tag, verb, device, ncrp_property, transaction_id, user_selector, array, payload):
    """Initialize the command object. 
    """

    #
    if tag is None or tag == '':
      self.tag = ''
    elif len(tag) <= 31 and tag.isalnum():
      self.tag = tag
    else:
      raise errors.NcrpSyntaxError

    # verb should be s or g, case insensitive. Mandatory
    if verb.upper() not in ["S", "G"]:
      raise errors.UnknownCmd
    self.verb = verb.upper()

    self.payload = [i.upper() for i in payload.strip(' ').split(',')]

    # device can either be a 32 bit unsigned integeror a string of up to 23 characters, starting with A-Z. Optional, if missing 0 is assumed
    if device is None or device == '':
      self.device = 0
    elif device.isnumeric() and int(device) < 2**32:
      self.device = int(device)
    elif device[0].isalpha() and device[0].isupper() and len(device) <= 23:
      self.device = device
    else:
      raise errors.NcrpSyntaxError

    # NCRP_property is not checked here, if it matched it should be a valid property. It may not exist in the device, but that is not an issue on the command level. Though, a property should be specified. Mandatory
    if ncrp_property is None or ncrp_property == '':
      raise errors.NcrpSyntaxError
    self.ncrp_property = ncrp_property.upper()

    # transaction_id should be an unsigned integer (no range specified). Optional
    if transaction_id is None or transaction_id == '':
      self.transaction_id = ''
    elif transaction_id.isnumeric():
      self.transaction_id = int(transaction_id)
    else:
      raise errors.NcrpSyntaxError

    # user can only be a number between 0 and 31. Optional
    if user_selector is None or user_selector == '':
      self.user_selector = ''
    elif user_selector.isnumeric() and int(user_selector) < 32 and int(user_selector) >= 0:
      self.user_selector = int(user_selector)
    else:
      raise errors.NcrpSyntaxError

    # array can be a single index, a start index, a range, or a range with a step
    # if array is not empty, strip square brackets and convert string to int array [3, 2]
    if array is None or array == '':
      self.array = []
    else:
      try:
        self.array = array.strip(' ').split(',')
        # if the second element of the arrey exists, we check if it is '', in that case it is a "from index to end" array
        if len(self.array) == 2 and self.array[1] == '':
          self.array = [int(self.array[0]), '']
        else:
          self.array = [int(i) for i in self.array]
          if len(self.array) > 1:
            # check if start < end
            if self.array[0] > self.array[1]:
              raise errors.BadArrayIdx
        if len(self.array) > 3:
          raise errors.BadArrayLen
      # not super happy about this handling of errors, but it works
      except (errors.BadArrayLen, errors.BadArrayIdx):
        raise
      except Exception as exc:
        raise errors.BadArrayIdx from exc

  def __str__(self):
    """Return a string representation of the command. The last piece is self.values for sets and self.options for sets"""
    return f'tag: {self.tag}, verb: {self.verb}, device: {self.device}, ncrp_property: {self.ncrp_property}, transaction_id: {self.transaction_id}, user_selector: {self.user_selector}, array: {self.array}, payload: {self.payload}'


# All commands receive an asynchronous response with the following structure:
# Successful set response:  $TAG .\n\n;
# Successful get response:  $TAG .\nproperty_data\n;
# Error response:  $TAG !\nnn_err msg\n;

class Response:
  "unify set, get and error responses in one class"

  def __init__(self, tag, error=None, data=None):
    self.tag = tag
    self.data = data
    self.error = error

  def __str__(self):
    # if error is ok_resp, return a successful response
    if self.error == errors.OkRsp or self.error is None:
      if self.data is None:
        return f'${self.tag} .\n\n;'
      else:
        data = str(self.data).strip("[]' ")
        return f'${self.tag} .\n{data}\n;'
    else:
      return f'${self.tag} !\n{self.error}\n;'
