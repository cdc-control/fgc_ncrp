"""Error codes and messages for the FGC NCRP protocol."""


class Error(Exception):
  """Base class for exceptions in this module."""

  def __init__(self, error_code, error_message, error_description):
    self.error_code = error_code
    self.error_message = error_message
    self.error_description = error_description

  def __str__(self):
    return f"{self.error_code} {self.error_message}"


class OkRsp(Error):
  """OK Response: Indicates that the command was executed successfully. This message is never actually seen, but it is implied when no error is reported."""

  def __init__(self):
    super().__init__(1, 'ok rsp', 'OK Response: Indicates that the command was executed successfully. This message is never actually seen, but it is implied when no error is reported.')


class UnknownCmd(Error):
  """Unknown command: Indicates that the command buffer didn't start with either "S " or "G " (or the lower case equivalents). This will not be seen when using the terminal with the editor enabled, as the editor will add "G " by default if neither "S " or "G " are found."""

  def __init__(self):
    super().__init__(9, 'unknown cmd', 'Unknown command: Indicates that the command buffer didn\'t start with either "S " or "G " (or the lower case equivalents). This will not be seen when using the terminal with the editor enabled, as the editor will add "G " by default if neither "S " or "G " are found.')


class GetNotPerm(Error):
  """Get Not Permitted: Indicates that the specified property does not have a get function."""

  def __init__(self):
    super().__init__(10, 'get not perm',
                     'Get Not Permitted: Indicates that the specified property does not have a get function.')


class SetNotPerm(Error):
  """Set Not Permitted: Indicates that the specified property does not have a set function. This means that the property is "read only", and is changed by some process related to external inputs."""

  def __init__(self):
    super().__init__(11, 'set not perm', 'Set Not Permitted: Indicates that the specified property does not have a set function. This means that the property is "read only", and is changed by some process related to external inputs.')


class NcrpSyntaxError(Error):
  """Syntax Error: Indicates that the command could not be parsed because: A symbol was too long; A symbol contained a space. Where a symbol is defined as a string of characters that form part of a property name, a get option or a property value."""

  def __init__(self):
    super().__init__(14, 'syntax error', 'Syntax Error: Indicates that the command could not be parsed because: A symbol was too long; A symbol contained a space. Where a symbol is defined as a string of characters that form part of a property name, a get option or a property value.')


class NoDelimiter(Error):
  """No Delimiter: Indicates that the end of the command was reached before a valid delimiter. Clearly this error can only occur when end-of-command is not a valid delimiter. It means that more parameters were expected but were not found."""

  def __init__(self):
    super().__init__(15, 'no delimiter', 'No Delimiter: Indicates that the end of the command was reached before a valid delimiter. Clearly this error can only occur when end-of-command is not a valid delimiter. It means that more parameters were expected but were not found.')


class NoSymbol(Error):
  """No Symbol: Indicates that a valid delimiter character was found before any symbol characters. This error will be returned if a symbol is required but not supplied. returned."""

  def __init__(self):
    super().__init__(16, 'no symbol', 'No Symbol: Indicates that a valid delimiter character was found before any symbol characters. This error will be returned if a symbol is required but not supplied. returned.')


class UnknownSymbol(Error):
  """Unknown Symbol: Indicates that a token did not match a known symbol within the relevent context. For example, in one context the token might be expected to be a top-level property name, in another it might be Get Option."""

  def __init__(self):
    super().__init__(17, 'unknown symbol', 'Unknown Symbol: Indicates that a token did not match a known symbol within the relevent context. For example, in one context the token might be expected to be a top-level property name, in another it might be Get Option.')


class BadInteger(Error):
  """Bad Integer: Indicates that a token expected to contain an integer value contained invalid characters. Note that unsigned integers may be entered in hex using the C prefix "Ox", however, this is not valid for signed integer properties which must be entered in decimal. This error will be returned if a hex value is entered for a signed integer property."""

  def __init__(self):
    super().__init__(18, 'bad integer', 'Bad Integer: Indicates that a token expected to contain an integer value contained invalid characters. Note that unsigned integers may be entered in hex using the C prefix "Ox", however, this is not valid for signed integer properties which must be entered in decimal. This error will be returned if a hex value is entered for a signed integer property.')


class BadFloat(Error):
  """Bad Float: Indicates that a token expected to contain a floating point value contained invalid characters."""

  def __init__(self):
    super().__init__(19, 'bad float',
                     'Bad Float: Indicates that a token expected to contain a floating point value contained invalid characters.')


class BadArrayIdx(Error):
  """Bad Array Index: Indicates that an invalid array index was found. This might be because the array parameters are out of range, or that an array specifier is not appropriate for the specified property, or the array specifier syntax was invalid."""

  def __init__(self):
    super().__init__(20, 'bad array idx', 'Bad Array Index: Indicates that an invalid array index was found. This might be because the array parameters are out of range, or that an array specifier is not appropriate for the specified property, or the array specifier syntax was invalid.')


class BadArrayLen(Error):
  """Bad Array Length: Indicates that an array length was not compatible with the requested operation."""

  def __init__(self):
    super().__init__(21, 'bad array len',
                     'Bad Array Length: Indicates that an array length was not compatible with the requested operation.')


class BadParameter(Error):
  """Bad Parameter: Indicates that a parameter was invalid in the context. This is a rather general error and can occur for many different reasons. Either a parameter was of invalid type, or perhaps a parameter was supplied when not required. Consult the syntax for the set/get function concerned."""

  def __init__(self):
    super().__init__(23, 'bad parameter', 'Bad Parameter: Indicates that a parameter was invalid in the context. This is a rather general error and can occur for many different reasons. Either a parameter was of invalid type, or perhaps a parameter was supplied when not required. Consult the syntax for the set/get function concerned.')


class OutOfLimits(Error):
  """Out Of Limits: Indicates that a number has been found which is either greater than the maximum or less than the minimum permitted in the context. This can also apply to setting reference where the reference will exceed the absolute limits (current in closed loop, voltage in open-loop). If the reference is PLEP, PPPL or TABLE, then REF.ERROR.INDEX will indicate which segment or point caused the error."""

  def __init__(self):
    super().__init__(28, 'out of limits', 'Out Of Limits: Indicates that a number has been found which is either greater than the maximum or less than the minimum permitted in the context. This can also apply to setting reference where the reference will exceed the absolute limits (current in closed loop, voltage in open-loop). If the reference is PLEP, PPPL or TABLE, then REF.ERROR.INDEX will indicate which segment or point caused the error.')


class BadState(Error):
  """Bad State: Indicates that the Set function could not be executed because the state of system was not appropriate."""

  def __init__(self):
    super().__init__(34, 'bad state',
                     'Bad State: Indicates that the Set function could not be executed because the state of system was not appropriate.')


class LogWaiting(Error):
  """Log Waiting: Indicates that the a log buffer is waiting to read out. This can be the post mortem log or the capture log following a cycle check. """

  def __init__(self):
    super().__init__(35, 'log waiting',
                     """Log Waiting: Indicates that the a log buffer is waiting to read out. This can be the post mortem log or the capture log following a cycle check.""")


class UnknownDevice(Error):
  """Unknown Device: Indicates that there is no device with the specified address."""

  def __init__(self):
    super().__init__(38, 'unknown dev',
                     'Unknown Device: Indicates that there is no device with the specified address.')


class DevNotReady(Error):
  """Device Not Ready: Indicates that the device is not ready to accept a command."""

  def __init__(self):
    super().__init__(39, 'dev not ready',
                     'Device Not Ready: Indicates that the device is not ready to accept a command.')


class Busy(Error):
  """Busy: Indicates that the device is busy and cannot accept a command at this time."""

  def __init__(self):
    super().__init__(40, 'busy',
                     'Busy: Indicates that the device is busy and cannot accept a command at this time.')


class NcrpNotImplemented(Error):
  """Not Implemented: Indicates that this feature is not yet implemented. For log readout, it would imply that either a DISABLED log was being read or no signals were requested. Both would imply a mistake in the software and the developers should be contacted."""

  def __init__(self):
    super().__init__(42, 'not impl', 'Not Implemented: Indicates that this feature is not yet implemented. For log readout, it would imply that either a DISABLED log was being read or no signals were requested. Both would imply a mistake in the software and the developers should be contacted.')


class NcrpIOError(Error):
  """I/O Error: Indicates that an I/O error occurred."""

  def __init__(self):
    super().__init__(45, 'io error', 'I/O Error: Indicates that an I/O error occurred.')


class IdFault(Error):
  """ID Fault: Indicates that the FGC was unable to recover ID and/or temperature information from devices on the Dallas buses."""

  def __init__(self):
    super().__init__(59, 'id fault',
                     'ID Fault: Indicates that the FGC was unable to recover ID and/or temperature information from devices on the Dallas buses.')


class UnknownProperty(Error):
  """Unknown property: Indicates that the command was rejected based upon rules defined in the RBAC system. This may be because an attempt is being made to access a non-existant property"""

  def __init__(self):
    super().__init__(66, 'unknown property', 'Unknown property: Indicates that the command was rejected based upon rules defined in the RBAC system. This may be because an attempt is being made to access a non-existant property')


class NullFailed(Error):
  """Null failed: Indicates that the offset nulling of the current calibrator failed."""

  def __init__(self):
    super().__init__(79, 'null failed',
                     'Null failed: Indicates that the offset nulling of the current calibrator failed.')
