# FGC Network Command/Response protocol

This file is part of the CDC Control project, but in principle it should be generic enough to allow to be used for anything.

Protocol specs mostly from: 
- https://gitlab.cern.ch/mcejp/fgcd2-command-parser/-/blob/master/SPEC.md
- https://wikis.cern.ch/pages/viewpage.action?pageId=70682032#FGCCommand/ResponseProtocol-NetworkCommandResponseProtocol(NCRP)
