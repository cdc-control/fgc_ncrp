"""This module contains the base property class for the NCRP protocol."""

import os

import ast

from typing import List, Union, Callable, Any, Optional
from . import errors


class Property:
  """This class is used to create a property object.

  The property object is used to store the property value, and to get and set the property value.

  Args:
      name (str): the name of the property. The name is converted to upper case.
      access (str): a string containing the access permissions of the property. The string is composed of the characters 'r', 'w', and 'p'. 'r' means that the property can be read, 'w' means that the property can be written, 'p' means that the property is persistent (saved in storage). For example, 'rw' means that the property can be read and written, but is not persistent.
      val_type (Callable): the type of the property value. This should be a callable type, for example int, float, str, etc.
      length (int, optional): the length of the property value. Defaults to 1.
      getters (Optional[List[Callable]], optional): a list of getter functions. Defaults to None.
      setters (Optional[List[Callable]], optional): a list of setter functions. Defaults to None. For derived classes, a setter can alter the value of the property, to change the value that will be set and stored.
      min_val (Optional[Union[Any, List[Any]]], optional): the minimum value of the property. Can be a list or a scalar. Defaults to None.
      max_val (Optional[Union[Any, List[Any]]], optional): the maximum value of the property. Can be a list or a scalar. Defaults to None.
      unit (str, optional): the unit of the property value. Defaults to "".
      description (str, optional): a description of the property. Defaults to "".
      default (Optional[Union[Any, List[Any]]], optional): the default value of the property. Can be a list or a scalar.  Defaults to None.
      configs_path (str, optional): the path to the configs directory. Defaults to "configs/".
      default_path (str, optional): the path to the defaults directory. Defaults to "defaults/".

  Raises:
      ValueError: raised if the access string contains an invalid character
      TypeError: raised if val_type is not a callable type    name: the name of the property



  """

  def __init__(self,
               name: str,
               access: str,
               val_type: Callable,
               length: int = 1,
               getters: Optional[List[Callable]] = None,
               setters: Optional[List[Callable]] = None,
               min_val: Optional[Union[Any, List[Any]]] = None,
               max_val: Optional[Union[Any, List[Any]]] = None,
               unit: str = "",
               description: str = "",
               default: Optional[Union[Any, List[Any]]] = None,
               configs_path: str = "configs/",
               default_path: str = "defaults/"
               ):
    # Validate access
    for char in access:
      if char not in ['r', 'w', 'p']:
        raise ValueError(
            "Invalid character in access string. Valid characters are 'r', 'w', and 'p'.")

    # Check if val_type is callable
    if not callable(val_type):
      raise TypeError("val_type should be a callable type.")

    self.name = name.upper()
    self.access = access
    self.val_type = val_type
    self.length = length
    self.getters = getters
    self.setters = setters

    # Initialize min and max values with proper length
    self.min_val = self._get_scalar_or_list(min_val)
    self.max_val = self._get_scalar_or_list(max_val)

    self.unit = unit
    self.description = description
    self.default = self._get_scalar_or_list(default or self.val_type())
    self.configs_path = configs_path
    self.default_path = default_path
    self.value = self._load_or_get_default_value()

  def _get_scalar_or_list(self, val: Union[Any, List[Any]]) -> List[Any]:
    """Helper method to create a list of correct length from a scalar or a list."""
    return [val for _ in range(self.length)] if not isinstance(val, list) else val

  def _load_or_get_default_value(self) -> List[Any]:
    """Load the value from the config file if it exists, otherwise return the default value."""
    if 'p' in self.access:
      config_file_path = os.path.join(self.configs_path, f"{self.name}")
      if os.path.exists(config_file_path):
        with open(config_file_path, 'r',  encoding='utf-8') as cfg_file:
          # return the array read from the file where we expect the array to be the result of str([1, 2, 3])
          return ast.literal_eval(cfg_file.read())
      default_file_path = os.path.join(self.default_path, f"{self.name}")
      if os.path.exists(default_file_path):
        with open(default_file_path, 'r', encoding='utf-8') as def_file:
          return ast.literal_eval(def_file.read())
    return self.default

  def _write_value(self, new_value: List[Any]) -> None:
    """Write the value to the config file."""
    config_file_path = os.path.join(self.configs_path, f"{self.name}")
    with open(config_file_path, 'w', encoding='utf-8') as cfg_file:
      cfg_file.write(str(new_value))

  def _expand_command_array(self, command_array: Union[None, List[Union[int, str]]]) -> slice:
    """Expand the array from the command and return a slice object"""
    if not command_array:
      return slice(None, None, None)
    if len(command_array) == 1 and isinstance(command_array[0], int):
      return slice(command_array[0], command_array[0]+1, None)
    if len(command_array) == 2 and isinstance(command_array[1], str) and command_array[1] == '':
      return slice(command_array[0], None, None)
    if len(command_array) == 2 and isinstance(command_array[0], int) and isinstance(command_array[1], int):
      return slice(command_array[0], command_array[1]+1, None)
    if len(command_array) == 3:
      return slice(command_array[0], command_array[1]+1, command_array[2])
    # if lenght is more than 3
    if len(command_array) > 3:
      raise errors.BadArrayLen
    raise errors.BadArrayIdx

  def _validate_array_slice(self, array_slice: slice) -> None:
    """Test if the array slice is valid, by checking if start and stop are within the array length. Also checks that start < stop."""
    if array_slice.start is not None and array_slice.start < 0:
      raise errors.BadArrayIdx
    if array_slice.stop is not None and array_slice.stop > self.length:
      raise errors.BadArrayIdx
    # validate that start < stop
    if array_slice.start is not None and array_slice.stop is not None and array_slice.start >= array_slice.stop:
      raise errors.BadArrayIdx

  def _get(self, array_slice: slice = slice(None, None, None)) -> List[Any]:
    """Internal method to get the property value. This method is called by the get method."""

    return [self.getters[idx]() if self.getters else self.value[idx] for idx in range(*array_slice.indices(self.length))]

  def _validate_payload_length(self, command_array: Union[None, List[Union[int, str]]], payload_length: int) -> None:
    """Validate that the payload length matches the command array."""
    array_slice = self._expand_command_array(command_array)
    expected_length = len(range(*array_slice.indices(self.length)))
    if expected_length != payload_length:
      print(
          f'expected_length: {expected_length}, payload_length: {payload_length}')
      raise errors.BadArrayLen

  def _validate_and_cast_payload(self, new_values: List[str]) -> List[Any]:
    """Validate and cast the payload values to the correct type."""
    casted_values = []

    for idx, val_str in enumerate(new_values):
      try:
        casted_val = self.val_type(val_str)
      except ValueError as exc:
        if self.val_type == float:
          raise errors.BadFloat from exc
        if self.val_type == int:
          raise errors.BadInteger from exc
        raise errors.BadParameter from exc

      if self.min_val[idx] is not None and casted_val < self.min_val[idx]:
        raise errors.OutOfLimits

      if self.max_val[idx] is not None and casted_val > self.max_val[idx]:
        raise errors.OutOfLimits

      casted_values.append(casted_val)

    return casted_values

  def _set(self, new_values: List[Any], array_slice: slice = slice(None)) -> None:
    """Internal method to set the property value."""
    # Validate and cast payload values
    new_values = self._validate_and_cast_payload(new_values)

    for idx, new_val in zip(range(*array_slice.indices(self.length)), new_values):
      if self.setters:
        self.setters[idx](new_val)
      else:
        self.value[idx] = new_val

    if 'p' in self.access:
      self._write_value(self.value)

  def get(self, command: Any) -> List[Any]:
    """Get the property value."""
    if 'r' not in self.access:
      raise errors.GetNotPerm
    array_slice = self._expand_command_array(command.array)
    self._validate_array_slice(array_slice)

    return self._get(array_slice)

  def set(self, command: Any) -> None:
    """Set the property value."""
    if 'w' not in self.access:
      raise errors.SetNotPerm
    array_slice = self._expand_command_array(command.array)
    self._validate_array_slice(array_slice)

    new_values = command.payload

    if not isinstance(new_values, list):
      raise errors.BadParameter

    # Validate payload length against command array
    self._validate_payload_length(command.array, len(new_values))

    self._set(new_values, array_slice)


class NotImplementedProperty(Property):
  "This class is used to create a property that is not implemented yet."

  def __init__(self, name):
    super().__init__(name, 'rw', str, description='This property is not implemented yet')

  def get(self, command):
    """Raise an error if the property is not implemented yet."""
    raise errors.NcrpNotImplemented

  def set(self, command):
    """Raise an error if the property is not implemented yet."""
    raise errors.NcrpNotImplemented
