import pytest
from ..fgc_property import Property
from .. import errors


def mock_command(payload=None, array=None):
  return type('MockCommand', (object,), {'payload': payload, 'array': array})


def test_property_initialization():
  prop = Property("test", "rwp", int, length=3, default=[1, 2, 3])
  assert prop.name == "TEST"
  assert prop.access == "rwp"
  assert prop.value == [1, 2, 3]


def test_scalar_property_initialization():
  prop = Property("test", 'rp', int, min_val=-32767, max_val=32768, default=0)
  assert prop.name == "TEST"
  assert prop.access == "rp"
  assert prop.value == [0]


def test_invalid_access():
  with pytest.raises(ValueError):
    Property("test", "rwx", int)


def test_invalid_val_type():
  with pytest.raises(TypeError):
    Property("test", "rw", "int")


def test_read_without_permission():
  prop = Property("test", "w", int)
  cmd = mock_command()
  with pytest.raises(errors.GetNotPerm):
    prop.get(cmd)


def test_write_without_permission():
  prop = Property("test", "r", int)
  cmd = mock_command(payload=[4])
  with pytest.raises(errors.SetNotPerm):
    prop.set(cmd)


def test_get_entire_array():
  prop = Property("test", "rw", int, length=5, default=[1, 2, 3, 4, 5])
  cmd = mock_command()
  assert prop.get(cmd) == [1, 2, 3, 4, 5]


def test_set_single_index():
  prop = Property("test", "rw", int, length=3, default=[1, 2, 3])
  cmd = mock_command(payload=["4"], array=[1])
  prop.set(cmd)
  assert prop.value == [1, 4, 3]


def test_get_single_index():
  prop = Property("test", "rw", int, length=3, default=[1, 2, 3])
  cmd = mock_command(array=[1])
  assert prop.get(cmd) == [2]


def test_get_range():
  prop = Property("test", "rw", int, length=5, default=[1, 2, 3, 4, 5])
  cmd = mock_command(array=[1, 3])
  assert prop.get(cmd) == [2, 3, 4]


def test_get_range_with_step():
  prop = Property("test", "rw", int, length=5, default=[1, 2, 3, 4, 5])
  cmd = mock_command(array=[0, 4, 2])
  assert prop.get(cmd) == [1, 3, 5]


def test_get_from_index_to_end():
  prop = Property("test", "rw", int, length=5, default=[1, 2, 3, 4, 5])
  cmd = mock_command(array=[2, ''])
  assert prop.get(cmd) == [3, 4, 5]


def test_bad_array_index():
  prop = Property("test", "rw", int)
  cmd = mock_command(array=[0, 'x'])
  with pytest.raises(errors.BadArrayIdx):
    prop.get(cmd)


def test_payload_not_list():
  prop = Property("test", "rw", int)
  cmd = mock_command(payload="4", array=[0])
  with pytest.raises(errors.BadParameter):
    prop.set(cmd)


def test_mismatched_payload_length():
  prop = Property("test", "rw", int, length=3, default=[1, 2, 3])
  cmd = mock_command(payload=["4", "5"], array=[1])
  with pytest.raises(errors.BadArrayLen):
    prop.set(cmd)


def test_min_max_boundaries():
  prop = Property("test", "rw", int, length=3, default=[
                  1, 2, 3], min_val=[0, 1, 2], max_val=[2, 3, 4])

  cmd = mock_command(payload=["3", "4", "5"], array=[0, 2])
  # This should raise a ValueError because values are out of bounds
  with pytest.raises(errors.OutOfLimits):
    prop.set(cmd)

  cmd = mock_command(payload=["0", "2", "3"], array=[0, 2])
  prop.set(cmd)
  # This should work as all values are within bounds
  assert prop.value == [0, 2, 3]


def test_failure_on_type_casting():
  prop = Property("test", "rw", int, length=3, default=[1, 2, 3])

  cmd = mock_command(payload=['a', 'b', 'c'], array=[0, 2])
  # This should raise a TypeError because of incorrect type casting
  with pytest.raises(Exception):
    prop.set(cmd)


def test_successful_persisting_to_files(tmp_path):
  prop = Property("test", "rwp", int, length=3, default=[
                  1, 2, 3], configs_path=str(tmp_path), default_path=str(tmp_path))

  cmd = mock_command(payload=["4", "5", "6"], array=[0, 2])
  prop.set(cmd)  # This should write to file
  assert prop.value == [4, 5, 6]

  # Verify that values are persisted to file
  with open(tmp_path / "TEST", 'r') as f:
    assert f.read().strip() == str([4, 5, 6])

  # Emulate loading from persisted file
  new_prop = Property("test", "rwp", int, length=3,
                      configs_path=str(tmp_path), default_path=str(tmp_path))
  assert new_prop.value == [4, 5, 6]

  # Emulate loading from default file when config file is absent
  (tmp_path / "TEST").unlink()  # Deleting the config file
  with open(tmp_path / "TEST", 'w') as f:
    f.write("[7, 8, 9]")
  new_prop2 = Property("test", "rwp", int, length=3,
                       configs_path=str(tmp_path), default_path=str(tmp_path))
  assert new_prop2.value == [7, 8, 9]
