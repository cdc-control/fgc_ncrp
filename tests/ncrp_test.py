"pytest tests for the NCRP command and properties"

# This file is part of the FGC NCRP
import pytest

from .. import fgc_property, ncrp


def test_cmd_set():
  cmd = ncrp.Command('', 's', '', 'foo.bar', '', '', '', '')
  assert cmd.verb == 'S'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.user_selector == ''


def test_cmd_get():
  cmd = ncrp.Command('', 'g', '', 'foo.bar', '', '', '', '')
  assert cmd.verb == 'G'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.user_selector == ''


def test_cmd_no_tag():
  cmd = ncrp.Command('', 'g', '', 'foo.bar', '', '', '', '')
  assert cmd.tag == ''
  assert cmd.verb == 'G'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.user_selector == ''


def test_cmd_tag():
  cmd = ncrp.Command('t4g', 's', '', 'foo.bar', '', '', '', '')
  assert cmd.tag == 't4g'
  assert cmd.verb == 'S'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.user_selector == ''


def test_cmd_none_empty():
  cmd = ncrp.Command('t4g', 's', None, 'foo.bar', None, None, None, '')
  assert cmd.tag == 't4g'
  assert cmd.verb == 'S'
  assert cmd.ncrp_property == 'FOO.BAR'


def test_cmd_verb_unknown():
  with pytest.raises(ncrp.errors.UnknownCmd):
    ncrp.Command('t4g', 'z', '', 'foo.bar', '', '', '', '')


def test_cmd_verb_too_long():
  with pytest.raises(ncrp.errors.UnknownCmd):
    ncrp.Command('t4g', 'set', '', 'foo.bar', '', '', '', '')


def test_cmd_tag_too_long():
  with pytest.raises(ncrp.errors.NcrpSyntaxError):
    ncrp.Command('too_long_tag_45678901234567890123',
                 's', 'foo', 'bar', '', '', '', '')


def test_cmd_only_verb():
  with pytest.raises(ncrp.errors.NcrpSyntaxError):
    ncrp.Command('t4g', 's', '', '', '', '', '', '')


def test_cmd_invalid_char():
  with pytest.raises(ncrp.errors.NcrpSyntaxError):
    ncrp.Command('t4g', 's', 'foo', 'bar', '', '', 'aaa%$', '')


def test_cmd_ncrp_property():
  cmd = ncrp.Command('t4g', 's', 'Foo.bar', 'bar', '', '', '', '')
  assert cmd.verb == 'S'
  assert cmd.device == 'Foo.bar'
  assert cmd.ncrp_property == 'BAR'
  assert cmd.user_selector == ''


def test_cmd_many_ncrp_property_user():
  cmd = ncrp.Command('t4g', 's', '', 'foo.bar.blorb.nap', '', '22', '', '')
  assert cmd.verb == 'S'
  assert cmd.ncrp_property == 'FOO.BAR.BLORB.NAP'
  assert cmd.user_selector == 22


def test_cmd_ncrp_property_user():
  cmd = ncrp.Command('t4g', 's', '', 'foo.bar', '', '1', '', '')
  assert cmd.verb == 'S'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.user_selector == 1


def test_cmd_all_empty():
  with pytest.raises(ncrp.errors.UnknownCmd):
    ncrp.Command('', '', '', '', '', '', '', '')


def test_cmd_numeric_device():
  cmd = ncrp.Command('t4g', 's', '12', 'foo.bar', '', '', '', '')
  assert cmd.verb == 'S'
  assert cmd.device == 12
  assert cmd.ncrp_property == 'FOO.BAR'


def test_cmd_text_device():
  cmd = ncrp.Command('t4g', 's', 'Foo', 'bar', '', '', '', '')
  assert cmd.verb == 'S'
  assert cmd.device == 'Foo'
  assert cmd.ncrp_property == 'BAR'


def test_cmd_numeric_transaction():
  cmd = ncrp.Command('t4g', 's', '', 'foo.bar', '12', '', '', '')
  assert cmd.verb == 'S'
  assert cmd.transaction_id == 12
  assert cmd.ncrp_property == 'FOO.BAR'


def test_cmd_text_transaction():
  with pytest.raises(ncrp.errors.NcrpSyntaxError):
    ncrp.Command('t4g', 's', '', 'foo.bar', 'Foo', '', '', '')


def test_cmd_numeric_user():
  cmd = ncrp.Command('t4g', 's', '', 'foo.bar', '', '12', '', '')
  assert cmd.verb == 'S'
  assert cmd.user_selector == 12
  assert cmd.ncrp_property == 'FOO.BAR'


def test_cmd_user_out_of_range():
  with pytest.raises(ncrp.errors.NcrpSyntaxError):
    ncrp.Command('t4g', 's', '', 'foo.bar', '', '99', '', '')


def test_cmd_text_user():
  with pytest.raises(ncrp.errors.NcrpSyntaxError):
    ncrp.Command('t4g', 's', '', 'foo.bar', '', 'Foo', '', '')


def test_cmd_array():
  cmd = ncrp.Command('t4g', 's', '', 'foo.bar', '', '', '1,2,3', '')
  assert cmd.verb == 'S'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.array == [1, 2, 3]
  cmd = ncrp.Command('t4g', 's', '', 'foo.bar', '', '', '1 ,2 ,3 ', '')
  assert cmd.array == [1, 2, 3]
  cmd = ncrp.Command('t4g', 's', '', 'foo.bar', '', '', '1   ,  2   ,   3', '')
  assert cmd.array == [1, 2, 3]


def test_cmd_array_index():
  cmd = ncrp.Command('t4g', 's', '', 'foo.bar', '', '', '1', '')
  assert cmd.verb == 'S'
  assert cmd.ncrp_property == 'FOO.BAR'
  assert cmd.array == [1]


def test_cmd_array_bad_length():
  with pytest.raises(ncrp.errors.BadArrayLen):
    ncrp.Command('t4g', 's', '', 'foo.bar', '', '', '1,2,3,4', '')


def test_cmd_array_bad_idx():
  with pytest.raises(ncrp.errors.BadArrayIdx):
    ncrp.Command('t4g', 's', '', 'foo.bar', '', '', '1,2,bcd', '')


def test_cmd_array_from_index():
  cmd = ncrp.Command('t4g', 's', '', 'foo.bar', '', '', '1,', '')
  assert cmd.array == [1, '']


def test_cmd_array_from_to_index():
  cmd = ncrp.Command('t4g', 's', '', 'foo.bar', '', '', '1,2', '')
  assert cmd.array == [1, 2]


def test_cmd_array_to_index():
  with pytest.raises(ncrp.errors.BadArrayIdx):
    ncrp.Command('t4g', 's', '', 'foo.bar', '', '', ',2', '')
