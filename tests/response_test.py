"""Pytest for the Response class of the NCRP."""
from .. import ncrp


def test_response_ok():
  resp = ncrp.Response('t4g', error=ncrp.errors.OkRsp)
  assert str(resp) == "$t4g .\n\n;"


def test_error_str():
  error_instance = ncrp.errors.NcrpSyntaxError()
  assert str(error_instance) == '14 syntax error'


def test_response_with_data():
  resp = ncrp.Response('', error=ncrp.errors.OkRsp, data='foo')
  assert str(resp) == "$ .\nfoo\n;"


def test_response_with_error():
  resp = ncrp.Response('', error=ncrp.errors.NcrpSyntaxError())
  assert str(resp) == "$ !\n14 syntax error\n;"
